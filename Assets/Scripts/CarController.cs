﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Dreamteck.Splines;

public class CarController : MonoBehaviour {

    public SplineFollower follower;
    public float yOffSet;
    public int numberOfLanes;
    public float trackWidth;

    public float switchSpeed;
    int moveRequest;
    int currentLane;
    int lastLane;
    bool canMove;
    float laneChangeTime;
    Vector2[] lanes;
    // Use this for initialization
    void Start() {
        lanes = new Vector2[numberOfLanes];
        laneChangeTime = 0;
        canMove = true;
        currentLane = 1;
        lastLane = currentLane;

        float laneWidth = trackWidth / numberOfLanes;
        for(int i = 1; i <= numberOfLanes; i++) {
            lanes[i - 1] = new Vector2(((i * laneWidth)-(laneWidth/2))-trackWidth/2 , yOffSet);
        }
    }

    // Update is called once per frame
    void Update() {        
        if (laneChangeTime <= 1) {
            follower.motion.offset = Vector2.Lerp(lanes[lastLane], lanes[currentLane], laneChangeTime);
            laneChangeTime = laneChangeTime + switchSpeed * Time.deltaTime;
        };
        if (laneChangeTime >= 1) {
           follower.motion.offset = lanes[currentLane];
        }
    }

    public void Action(int actionIndicator) {
        //Debug.Log(Enum.GetName(typeof(InputManager.action), actionIndicator));
        if (actionIndicator == 2 && currentLane != numberOfLanes-1 ) {
            laneChangeTime = 0f;
            lastLane = currentLane;
            currentLane = currentLane + 1;
        } else if (actionIndicator == 1 && currentLane != 0) {
            laneChangeTime = 0f;
            lastLane = currentLane;
            currentLane = currentLane - 1;
        } 
    }
}