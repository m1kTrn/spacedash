﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Dreamteck.Splines;
public class Controller : MonoBehaviour {

    public SplineFollower follower;
    public Vector2[] lanes;
    public float switchSpeed;
    int currentLane;
    int lastLane;
    bool canMove;
    float t;
    // Use this for initialization
    void Start () {
        t = 0;
        canMove = true;
        currentLane = 1;
        lastLane = 1;
        Debug.Log(lanes[0]);
    }

    // Update is called once per frame
    void Update() {
        

        int move = (int) Input.GetAxisRaw("Horizontal");
        if (move == 0) {
            canMove = true;
        }
        if (currentLane == 1 && canMove) {            
            if (move == 1) {
                t = 0f;
                lastLane = currentLane;
                currentLane = 2;
                canMove = false;
            }
            else if (move == -1) {
                t = 0f;
                lastLane = currentLane;
                currentLane = 0;
                canMove = false;
            }            
        }
        if (( currentLane == 0 && move == 1) || currentLane == 2 && move == -1) {
            t = 0f;
            lastLane = currentLane;
            currentLane = 1;
            canMove = false;                        
        }
        if(t <= 1) {
            follower.motion.offset = Vector2.Lerp(lanes[lastLane], lanes[currentLane], t);
            t = t + switchSpeed * Time.deltaTime;
        };
        if (t >=1) {
            follower.motion.offset = lanes[currentLane];
        }
        
        
    }

    
}
