﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Dreamteck.Splines;


public class TrackGenerator : MonoBehaviour {

    public int nmbrofpoints = 10;

	public void BuildTrack () {
        SplineComputer computer = GetComponent<SplineComputer>();
        SplinePoint[] points = new SplinePoint[nmbrofpoints];
        Debug.Log(points.Length);
        for (int i = 0; i < points.Length; i++) {
            points[i] = new SplinePoint(new Vector3(0, (i*10), (i * 100)), Vector3.forward, Vector3.up, 1f, Color.black);            
        }
        
        computer.SetPoints(points);
        SplineResult[] results = new SplineResult[computer.iterations];
        computer.Evaluate(ref results);

        computer.RebuildImmediate();
        
            foreach (SplinePoint point in points) {
            Debug.Log(point.position);
        }
        

    }
	
	// Update is called once per frame
	
}
