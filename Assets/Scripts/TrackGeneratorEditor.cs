﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(TrackGenerator))]
public class TrackGeneratorEditor : Editor {

    
    public override void OnInspectorGUI() {
        DrawDefaultInspector();

        TrackGenerator trackGenerator = (TrackGenerator)target;
        if(GUILayout.Button("Generate Track")) {
            trackGenerator.BuildTrack();
        }
    }
}
