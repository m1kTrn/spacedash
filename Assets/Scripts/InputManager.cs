﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour {

    public CarController carController;
    public enum action {
        nothing, moveLeft, moveRight, Jump, Boost
    };
    bool canMove;
	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
        int horiMove = (int)Input.GetAxisRaw("Horizontal");
        if (horiMove == 0) {
            canMove = true;
        }
        if (canMove) {
            if (horiMove == 1) {
                carController.Action((int) action.moveRight);
                canMove = false;
            } else if (horiMove == -1) {
                carController.Action((int) action.moveLeft);
                canMove = false;
            } 
        }        
    }    
}
